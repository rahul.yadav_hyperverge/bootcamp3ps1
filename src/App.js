import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { AuthPage, HomePage, PostPage, ProfilePage } from './index';
import Menu from './components/organisms/PageMenu';
import { useEffect, useState } from 'react';


function getToken() {
	const tokenString = sessionStorage.getItem('token');
	const userToken = JSON.parse(tokenString);
	return userToken
}

function App() {
	const [tokenState, setTokenState] = useState();
	function setToken(userToken) {
		sessionStorage.setItem('token', JSON.stringify(userToken));
		setTokenState(userToken);
	}
	useEffect(() => {
		setTokenState(getToken());
	}, []);

	if (!tokenState) {
		return <AuthPage setToken={setToken} />
	}
	return (
		<div className="Main">
			<Router>
				<Menu />
				<Switch>
					<Route path="/" exact component={() => <HomePage token={tokenState}/>} />
					<Route path="/post" exact component={() => <PostPage token={tokenState} />} />
					<Route path="/profile" exact component={() => <ProfilePage token={tokenState} setTokenState={setTokenState} />} />
				</Switch>
			</Router>
		</div>
	);
}
export default App;