import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
export { default as HomePage } from './components/pages/Home/index';
export { default as PostPage } from './components/pages/Post/index';
export { default as ProfilePage } from './components/pages/Profile/index';
export { default as AuthPage } from './components/pages/Auth/index';

ReactDOM.render( <React.StrictMode >
    <App /></React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();