import Menu from '../../organisms/AuthMenu';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import SignUp from '../../organisms/SignUp';
import SignIn from '../../organisms/SignIn';

function AuthPage({ setToken }){
    return (
        <div className="auth-page">
            <Router>
                <Menu />
                <Switch>
                    <Route path="/" exact component={() => <SignIn setToken={ setToken }/>} />
                    <Route path="/SignUp" exact component={() => <SignUp setToken={ setToken }/>} />
                </Switch>
            </Router>
        </div>
    );
}
export default AuthPage;