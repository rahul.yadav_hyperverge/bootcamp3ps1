import '../../../App.css';
import '../../../styles/HomePage.css'
import Posts from '../../organisms/Posts';
import Friends from '../../organisms/Friends';
function HomePage({ token }) {
    return (
        <div className="content">
            <Posts token={token} />
            <Friends token={token} />
        </div>
    );
}
export default HomePage;