import '../../../App.css';
import '../../../styles/PostPage.css';
import Button from '../../atoms/Button';
import ProgressBar from '../../atoms/ProgressBar';
import usePost from '../../logics/usePost';

function PostPage({ token }) {    
    const { displayLoader, handleSubmit, setImageAsFile, setImageDescription  } = usePost(token);
    const handleImageAsFile = (e) => {
        const image = e.target.files[0];
        setImageAsFile(image);
    }

    return (
        <div className="content">
            <div className="postpage-box">
                <input id="selectImage" className="select-image" type="file" onChange={handleImageAsFile} />
                <div className="display-box">
                    <div className="display-box-name">DESCRIPTION</div>
                    <textarea id="description-box" type="text" size="35" className="display-box-value" name="description" placeholder="Enter something" onChange={e => setImageDescription(e.target.value)} />
                </div>
                <Button value="UPLOAD POST" width="80%" color="#FF3D2A" style="fill" onClick={handleSubmit} />
                <ProgressBar display={displayLoader}/>
            </div>
        </div>
    );
}
export default PostPage;