import '../../../App.css';
import '../../../styles/ProfilePage.css';
import CircularImage from '../../atoms/Image';
import DisplayBox from '../../atoms/DisplayBox';
import Button from '../../atoms/Button';
import { IoMdLogOut } from 'react-icons/io'
import ProgressBar from '../../atoms/ProgressBar';
import useProfile from '../../logics/useProfile';

function Profile({ token, setTokenState }) {
    const { name, phonenumber, email, handleSubmit, setImageAsFile, imageAsUrl, editOption, displayLoader, setEditOption, setPhoneNumber, setEmail } = useProfile(token);
    const handleImageAsFile = (e) => {
        const image = e.target.files[0];
        setImageAsFile(image);
    }
    const handleLogout = e => {
        sessionStorage.removeItem('token');
        setTokenState('');
        window.location.pathname = '/';
    }
    return (
        <div className="content">
            <div className="profile-image">
                <div className="image-box">
                    <div className="image">
                        <CircularImage url={imageAsUrl} />
                        <div className="logout-box">
                            <div className="logout">
                                <IoMdLogOut onClick={handleLogout} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="profile-details">
                <div className="profile-box">
                    <DisplayBox name="NAME" value={`${name}`} readonly="readonly" />
                    <div className="display-box">
                        <div className="display-box-name">UPLOAD PROFILE IMAGE</div>
                        <input className="display-box-value" type="file" onChange={handleImageAsFile} />
                    </div>
                    <div className="horziontal-line"></div>
                    <DisplayBox name="PHONE NUMBER" value={`${phonenumber}`} readonly={editOption} onChange={e => setPhoneNumber(e.target.value)} />
                    <DisplayBox name="EMAIL" value={`${email}`} readonly={editOption} onChange={e => setEmail(e.target.value)} />
                    <Button value="EDIT" thickness="2px" color="#4267B2" width="80%" style="outline" onClick={() => setEditOption('')} />
                    <Button value="SAVE" width="80%" color="#FF3D2A" style="fill" onClick={handleSubmit} />
                    <ProgressBar display={displayLoader} />
                </div>
            </div>
        </div>
    );
}
export default Profile;