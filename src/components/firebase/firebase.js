import firebase from 'firebase/app'
import 'firebase/storage'
var firebaseConfig = {
    apiKey: "AIzaSyAOBQSruELhtosLHOZsc9U0fu5nsuBshtc",
    authDomain: "social-media-app-41936.firebaseapp.com",
    projectId: "social-media-app-41936",
    storageBucket: "social-media-app-41936.appspot.com",
    messagingSenderId: "968780521604",
    appId: "1:968780521604:web:dd63b55551b3a9746562d1",
    measurementId: "G-G3JRKC13W3"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  const storage = firebase.storage()
  // firebase.analytics();

export  { storage, firebase as default }