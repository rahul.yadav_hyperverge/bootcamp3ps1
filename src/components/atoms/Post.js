import '../../App.css';
import '../../styles/Post.css';
import React, { useState, useEffect } from 'react';
import { BiComment } from 'react-icons/bi';
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai'
import { RiSendPlaneLine, RiSendPlaneFill } from 'react-icons/ri';
import CircularImage from './Image';
import { MdModeComment } from 'react-icons/md';
import Comment from './Comment'

async function likeStatusUpdate(token, status, postId) {
    return fetch('http://localhost:5000/api/posts/post/addLike', {
        method: 'POST',
        headers: {
            authorization: token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ status: status, postId: postId })
    })
        .then(data => data.json());
}
async function putComment({token,postId,postingComment}) {
    return fetch('http://localhost:5000/api/posts/post/addComment', {
        method: 'POST',
        headers: {
            authorization: token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ postId: postId, comment: postingComment })
    })
        .then(data => data.json());
}
function Post({ data, token }) {
    const post = data[0];
    const user = data[0].createdBy;
    const postId=post._id;
    const [showComments , setShowComments] = useState('none');
    const [postingComment , setPostingComment]= useState();
    const [comments, setComments] = useState([]);
    const [likeCount, setLikeCount] = useState(post.likes);
    let [btnStatus, setBtnState] = useState({ 1: data[1], 2: false, 3: false });

    async function getComments({token,postId}) {
        return fetch('http://localhost:5000/api/posts/post/comments', {
            method: 'POST',
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ postId: postId})
        })
            .then(async (data) => {
                data= await data.json();
                setComments(data);
            });
    }
    const handleComment = async e =>{
        document.getElementById(`comment-input-${postId}`).value = '';
        await putComment({token,postId,postingComment});
    }
    useEffect(() => {
        getComments({token,postId});
    }, [btnStatus[2]])



    function Liked() {
        setBtnState({ 1: btnStatus[1] ^ true, 2: btnStatus[2], 3: btnStatus[3] });
        likeStatusUpdate(token, !btnStatus[1], postId);
        if(!btnStatus[1]){
            setLikeCount(likeCount+1);
        }else{
            setLikeCount(likeCount-1);
        }
    };
    function commented() {
        setBtnState({ 1: btnStatus[1], 2: btnStatus[2] ^ true, 3: btnStatus[3] });
        if(showComments === 'none')
            setShowComments('block');
        else
            setShowComments('none');
    };
    function sended() {
        setBtnState({ 1: btnStatus[1], 2: btnStatus[2], 3: btnStatus[3] ^ true });
    };
    let like, comment, send;
    like = !btnStatus[1] ? <AiOutlineHeart onClick={Liked} /> : <AiFillHeart onClick={Liked} />;
    comment = !btnStatus[2] ? <BiComment onClick={commented} /> : <MdModeComment onClick={commented} />;
    send = !btnStatus[3] ? <RiSendPlaneLine onClick={sended} /> : <RiSendPlaneFill onClick={sended} />;
    return (
        <div className="post-box">
            <div className="post-user-details">
                <div className="post-user-image">
                    <CircularImage url={user.photoUrl} />
                </div>
                <div className="post-user-info">
                    <div className="post-user-name">
                        {user.name}
                    </div>
                    <div className="post-upload-time">
                        1 hr ago • Gurgaon
                    </div>
                </div>
            </div>
            <div className="post-image" style={{ backgroundImage: `url(${post.postLink})` }}></div>
            <div className="post-info">
                <div className="post-options">
                    <div className="options">
                        {like}
                    </div>
                    <div className="options">
                        {comment}
                    </div>
                    <div className="options">
                        {send}
                    </div>
                </div>
                <div className="post-likes">{likeCount} Likes</div>
                <div className="post-description">{post.postDescription}</div>
                <div className="all-comments" style={{display:`${showComments}`}}>
                    {comments.map(data => (
                        <Comment key={data[2]} data={data}/>
                    ))}
                </div>
                <div className="comment-section">
                    <div className="comment-section-box">
                        <input id={`comment-input-${postId}`} type="text" placeholder="Add a comment..." onChange={e => setPostingComment(e.target.value)}/>
                        <div className="comment-post-box" onClick={handleComment}>Post</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Post;