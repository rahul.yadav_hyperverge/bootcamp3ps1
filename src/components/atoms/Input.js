import '../../styles/Input.css';
function Input(props){
    return (
        <div className="auth-input-box">
            <div className="auth-input-display">{props.label}</div>
            <input className="auth-input-value" placeholder={`${props.label}`} type={`${props.type}`} onChange={props.onChange}></input>
        </div>
    );
}
export default Input;