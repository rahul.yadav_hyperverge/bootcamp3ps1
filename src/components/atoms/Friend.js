import '../../App.css';
import '../../styles/Friend.css';
import CircularImage from './Image';
import { RiUserAddLine } from 'react-icons/ri';
import useFriend from '../logics/useFriend';
import { useState } from 'react';
function Friend({ data, token }) {
    const friendId = data._id;
    const { addFriend } = useFriend();
    const [addFriendBtn, setAddFriendBtn] = useState('block');

    const handleFriendShip = async e => {
        setAddFriendBtn('none');
        const data = { friendId: friendId }
        const result = await addFriend({ token, data });
    }
    return (
        <div className="friend-box">
            <div className="user-friend-image">
                <CircularImage url={`${data.photoUrl}`} />
            </div>
            <div className="user-friend-details">
                <div className="user-friend-name">{data.name}</div>
                <div className="user-friend-account">{data.name}.00</div>
            </div>
            <div className="add-friend">
                <RiUserAddLine onClick={handleFriendShip} style={{ display: `${addFriendBtn}` }} />
            </div>
        </div>
    );
}
export default Friend;