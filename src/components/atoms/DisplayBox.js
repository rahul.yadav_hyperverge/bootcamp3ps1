import '../../App.css';
import '../../styles/DisplayBox.css';

function DisplayBox(props){
    return (
        <div className="display-box">
            <div className="display-box-name">{props.name}</div>
            <input className="display-box-value" name={`${props.name}`} placeholder={`${props.value}`} readOnly={`${props.readonly}`} onChange={props.onChange}/>
        </div>
    );
}
export default DisplayBox;