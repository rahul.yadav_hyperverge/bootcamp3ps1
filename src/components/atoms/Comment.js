import '../../App.css';
import '../../styles/Comment.css'

function Comment({data}) {
    return (
        <div className="comment-box">
            <div className="comment-user">{data[0]}</div>
            <div className="comment">{data[1]}</div>
        </div>
    );
}
export default Comment;