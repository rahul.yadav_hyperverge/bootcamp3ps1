import '../../styles/ProgressBar.css';
import '../../App.css';

function ProgressBar({display}) {
    return (
        <div className="progress-bar" style={{display:`${display}`}}>
            <div className="progress-bar-box">
                <progress></progress>
            </div>
        </div>
    );
}
export default ProgressBar;