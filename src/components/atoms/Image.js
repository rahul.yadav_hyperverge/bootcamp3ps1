import '../../App.css';
import '../../styles/Image.css'

function Image(props) {
    return (
        <div className="circular-image" style={{backgroundImage: `url(${props.url})`,backgroundSize:'cover'}}></div>
    );
}
export default Image;