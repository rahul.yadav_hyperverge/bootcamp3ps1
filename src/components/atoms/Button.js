import '../../styles/Button.css';

function Button(props) {
    const fillStyle = { backgroundColor: props.color, width: props.width, color: `white` };
    const outlineStyle = { border: `solid ${props.thickness} ${props.color}`, width: props.width, color: props.color };
    return (
        <div className="custom-button-box" style={props.style === 'fill' ? fillStyle : outlineStyle} onClick={props.onClick}>
            {props.value}
        </div>
    );
}
export default Button;