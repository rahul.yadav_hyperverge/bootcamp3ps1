import '../../styles/AuthMenu.css';
import { Link, withRouter } from "react-router-dom";

function Menu() {
    return (
        <div className="auth-menu">
            <div className="auth-menu-box">
                <div className="auth-logo">
                    <div className="logo-box">
                        <div className="company-name">Hypergram</div>
                        <div className="logo-divider">|</div>
                        <div className="company-type">MEDIA</div>
                    </div>
                </div>
                <div className="auth-btns-box">
                    <div className="auth-btns">
                        <div className="auth-links">
                            <Link to ="/" style={{textDecoration:'none',padding:'10px'}}> Sign In</Link>
                            <Link to="/SignUp" style={{textDecoration:'none',padding:'10px'}}>
                                Sign Up
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default withRouter(Menu);