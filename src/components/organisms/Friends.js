import '../../App.css'
import '../../styles/Friends.css';
import Friend from '../atoms/Friend';
import CircularImage from '../atoms/Image';
import useFriends from '../logics/useFriends';

function Friends({ token }) {
    const { name , imageAsUrl , friends } = useFriends(token);

    return (
        <div className="Stories">
            <div className="user-profile">
                <div className="user-image">
                    <CircularImage url={`${imageAsUrl}`} />
                </div>
                <div className="user-info">
                    <div className="user-account">{name}.00</div>
                    <div className="user-name">{name}</div>
                </div>
            </div>
            <div className="text-stories">Recommendations</div>
            <div className="user-stories">
                {friends.map(data => (
                    <Friend data={data} key={data._id} token={token} />
                ))}
            </div>
        </div>
    );
}
export default Friends;