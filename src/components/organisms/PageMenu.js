import '../../App.css';
import '../../styles/PageMenu.css';
import React, { useState , useEffect } from 'react';
import { BiUserCircle } from 'react-icons/bi';
import { AiOutlineHome , AiFillHome } from 'react-icons/ai';
import { IoMdAddCircleOutline, IoMdAddCircle } from 'react-icons/io';
import { RiSendPlaneLine , RiSendPlaneFill} from 'react-icons/ri';
import { Link, withRouter } from "react-router-dom";
import { FaUserCircle } from 'react-icons/fa';
function Menu() {
    const currentPath = window.location.pathname;
    let [ menuState , setMenuState ] = useState({});
    useEffect(()=>{
        if(currentPath === '/')
            setMenuState({1:true,2:false,3:false,4:false});
        else if(currentPath === '/Post')
            setMenuState({1:false,2:true,3:false,4:false});
        else if(currentPath === '/message')
            setMenuState({1:false,2:false,3:true,4:false});
        else
            setMenuState({1:false,2:false,3:false,4:true});
    },[]);
    function home_selected(){
        setMenuState({1:true,2:false,3:false,4:false});
    };
    function post_selected(){
        setMenuState({1:false,2:true,3:false,4:false});
    };
    function message_selected(){
        setMenuState({1:false,2:false,3:true,4:false});
    };
    function profile_selected(){
        setMenuState({1:false,2:false,3:false,4:true});
    };
    let home,post,message,profile;
    home = !menuState[1]?<AiOutlineHome onClick={home_selected} />:<AiFillHome onClick={home_selected} />;
    post = !menuState[2]?<IoMdAddCircleOutline onClick={post_selected} />:<IoMdAddCircle onClick={post_selected} />;
    message = !menuState[3]?<RiSendPlaneLine onClick={message_selected} />:<RiSendPlaneFill onClick={message_selected} />;
    profile = !menuState[4]?<BiUserCircle onClick={profile_selected} />:<FaUserCircle onClick={profile_selected} />;
    return (
        <div className="menu-box">
            <div className="menu-inner">
                <div className="logo">Hypergram</div>
                <div className="search">
                    <div className="search-box">
                        Search
                    </div>
                </div>
                <div className="menu-items">
                    <div className="items-box">
                        <Link to="/">
                            <div className="items">
                                {home}
                            </div>
                        </Link>
                        <Link to="/post">
                            <div className="items">
                                {post}
                            </div>
                        </Link>
                        <Link to="/message">
                            <div className="items">
                                {message}
                            </div>
                        </Link>
                        <Link to="/profile">
                            <div className="items">
                                {profile}
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default withRouter(Menu);