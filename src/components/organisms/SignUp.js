import { useState } from 'react/cjs/react.development';
import '../../styles/Auth.css';
import '../../App.css';
import Input from '../atoms/Input';
import Button from '../atoms/Button';
import PropTypes from 'prop-types';
import signUp from '../logics/useSignUp';

function SignUp({ setToken }) {
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();
        const data = { name, email, password };
        const { token } = await signUp(data);
        setToken(token);
        window.location.pathname = '/';
    }

    return (
        <div className="auth-area">
            <div className="box-area">
                <div className="welcome-message">
                    Welcome Back
                </div>
                <Input label="Name" type="text" onChange={e => setName(e.target.value)} />
                <Input label="Email Address" type="email" onChange={e => setEmail(e.target.value)} />
                <Input label="Password" type="Password" onChange={e => setPassword(e.target.value)} />
                <br />
                <Button value="Sign Up" width="100%" color="#FF3D2A" style='fill' onClick={handleSubmit} />
            </div>
        </div>
    );
}
export default SignUp;

SignUp.propTypes = {
    setToken: PropTypes.func.isRequired
}