import '../../App.css';
import '../../styles/Posts.css';
import Post from '../atoms/Post';
import usePosts from '../logics/usePosts';


function Posts({ token}) {
    const { allPosts } = usePosts(token);
    
    return (
        <div className="Posts">
            {allPosts.map(data => (
                <Post data={data} key={`${data[0]._id}`} token={token} />
            ))}
        </div>
    ); 

}

export default Posts;