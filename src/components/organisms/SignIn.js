import '../../styles/Auth.css';
import '../../App.css';
import Input from '../atoms/Input';
import Button from '../atoms/Button';
import PropTypes from 'prop-types';
import { useState } from 'react';
import signIn from '../logics/useSignIn';

function SignIn({ setToken }) {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const handleSubmit = async e => {
        e.preventDefault();
        const data = { email, password };
        const { token } = await signIn(data);
        setToken(token);
    }

    return (
        <div className="auth-area">
            <div className="box-area">
                <div className="welcome-message">
                    Welcome Back
                </div>
                <Input label="Email Address" type="email" onChange={e => setEmail(e.target.value)} />
                <Input label="Password" type="Password" onChange={e => setPassword(e.target.value)} />
                <br />
                <Button value="Sign In" width="100%" color="#FF3D2A" style="fill" onClick={handleSubmit} />
            </div>
        </div>
    );
}
export default SignIn;

SignIn.propTypes = {
    setToken: PropTypes.func.isRequired
}