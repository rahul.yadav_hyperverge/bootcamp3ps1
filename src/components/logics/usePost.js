import { storage } from '../firebase/firebase';
import { useState } from 'react';
import useAPI from './useAPI';

const usePost = (token) => {
    const apiCall = useAPI();
    const [displayLoader, setDisplayLoader] = useState('none');
    const [imageAsFile, setImageAsFile] = useState('');
    const [imageDescription, setImageDescription] = useState('');
    const handleSubmit = e => {
        setDisplayLoader('block');
        e.preventDefault();
        console.log('start of upload');

        if (imageAsFile === '' || imageDescription === '') {
            console.error(`not an image, the image file is a ${typeof (imageAsFile)} or Description is empty`);
            return;
        }
        const uploadTask = storage.ref(`/posts/${imageAsFile.name}`).put(imageAsFile);
        //initiates the firebase side uploading 
        uploadTask.on('state_changed',
            (snapShot) => {
                console.log(snapShot)
            }, (err) => {
                console.log(err)
            }, () => {
                storage.ref('posts').child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        console.log(fireBaseUrl);
                        const data = { postLink: fireBaseUrl, postDescription: imageDescription };
                        apiCall({ token, data, path: 'posts/addPost', method: 'POST' })
                        setDisplayLoader('none');
                        document.getElementById('description-box').value = '';
                        document.getElementById('selectImage').value = '';
                    })
            })
    }
    return { handleSubmit, displayLoader, setImageAsFile, setImageDescription };
}
export default usePost;