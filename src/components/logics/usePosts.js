import { useState, useEffect } from 'react';
import useAPI from './useAPI';

const usePosts = (token) => {
    const apiCall = useAPI();
    const [allPosts, setAllPosts] = useState([]);

    useEffect(async () => {
        const result = await apiCall({token,method:'GET',path:'posts/'});
        setAllPosts(result);
    }, [])

    return { allPosts };
};

export default usePosts;