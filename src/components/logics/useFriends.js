import { useEffect, useState } from "react";
import useAPI from './useAPI';

const useFriends = (token) => {
    const apiCall = useAPI();
    const [friends, setFriends] = useState([]);
    const [name, setName] = useState('');
    const [imageAsUrl, setImageAsUrl] = useState('');
    const getFriends = async (token) => {
        const result = await apiCall({ token, path: 'friend/suggestions', method: 'GET' });
        return result;
    }
    const getProfile = async (token) => {
        const result = await apiCall({ token, path: 'profile/getProfile', method: 'GET' });
        return result;
    }
    useEffect(async () => {
        const data = await getFriends(token);
        const userProfile = await getProfile(token);
        setName(userProfile.name);
        setImageAsUrl(userProfile.photoUrl);
        setFriends(data);
    },[]);
    
    return { friends, name, imageAsUrl };
}
export default useFriends;