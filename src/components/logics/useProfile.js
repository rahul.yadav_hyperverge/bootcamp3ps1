import { storage } from '../firebase/firebase';
import { useState, useEffect } from 'react';
import useAPI from './useAPI';

const useProfile = (token) => {
    const apiCall = useAPI();
    const [id, setId] = useState('')
    const [name, setName] = useState('');
    const [phonenumber, setPhoneNumber] = useState('');
    const [email, setEmail] = useState('');
    const [editOption, setEditOption] = useState('readonly');
    const [imageAsFile, setImageAsFile] = useState('')
    const [imageAsUrl, setImageAsUrl] = useState('')
    const [displayLoader, setDisplayLoader] = useState('none');
    const handleGetProfile = async () => {
        const userProfile = await apiCall({ token, method: 'GET', path: 'profile/getProfile' });
        setName(userProfile.name);
        setPhoneNumber(userProfile.phonenumber);
        setEmail(userProfile.email);
        setImageAsUrl(userProfile.photoUrl);
        setId(userProfile._id);
    }
    const handleSubmit = e => {
        setDisplayLoader('block');
        e.preventDefault()
        console.log('start of upload')

        if (imageAsFile === '') {
            const data = { email: email, phonenumber: phonenumber, photoUrl: imageAsUrl };
            const result = apiCall({ token, data, method: 'PUT', path: 'profile/updateProfile' })
            setTimeout(() => { setDisplayLoader('none') }, 1000);
            return;
        }
        const uploadTask = storage.ref(`/profilePhotos/${id}/${imageAsFile.name}`).put(imageAsFile)
        uploadTask.on('state_changed',
            (snapShot) => {
                // console.log(snapShot)
            }, (err) => {
                console.log(err)
            }, () => {
                storage.ref(`/profilePhotos/${id}`).child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setImageAsUrl(fireBaseUrl);
                        setEditOption('readonly');
                        const data = { email: email, phonenumber: phonenumber, photoUrl: fireBaseUrl };
                        const result = apiCall({ token, data, method: 'PUT', path: 'profile/updateProfile' })
                        setDisplayLoader('none');
                    })
            })
    }
    useEffect(() => {
        handleGetProfile();
    }, []);
    return { handleGetProfile, name, phonenumber, email, handleSubmit, setImageAsFile, imageAsUrl, editOption, displayLoader, setEditOption, setEmail, setPhoneNumber };
};
export default useProfile;