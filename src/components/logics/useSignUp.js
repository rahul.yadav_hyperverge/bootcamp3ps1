import useAPI from './useAPI';

const SignUp = async (data) => {
    const apiCall = useAPI();
    const result = await apiCall({data,path:'auth/register',method:'POST'});
    return result;
}

export default SignUp;