import useAPI from './useAPI';

const SignIn = async (data) => {
    const apiCall = useAPI();
    const result = await apiCall({data,path:'auth/login',method:'POST'});
    return result;
}

export default SignIn;