import useAPI from './useAPI';

const useFriend = () => {
    const apiCall = useAPI();
    const addFriend = async ({ token, data }) => {
        const result = await apiCall({ token, data, method:'POST', path:'friend/addFriend' });
        return result;
    }
    return { addFriend };
};
export default useFriend;