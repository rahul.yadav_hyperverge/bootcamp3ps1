const router = require('express').Router();
const Joi = require('joi');
const verify = require('../middleware/verifyToken');
const { friendSuggestion, addFriend } = require('../controllers/profile');

router.get('/suggestions', verify, async (req, res) => {
    try {
        const result = await friendSuggestion(req);
        return res.send(result);
    }
    catch (err) {
        return res.status(400).send('something went wrong when getting the suggested friends');
    }
})
router.post('/addFriend', verify, async (req, res) => {
    try{
        await addFriend(req,res);
        return res.send('user added as your friend');
    }
    catch(err){
        return res.status(400).send('error while adding user as a friend');
    }
})

module.exports = router;