const router = require('express').Router();
const { register, login } = require('../controllers/auth');
const jwt = require('jsonwebtoken');

router.post('/register', async (req,res)=>{
    try{
        const user = await register(req.body);
        const savedUser = await user.save();
        const token = jwt.sign({ _id:user._id },process.env.TOKEN_SECRET);
        return res.header('Auth-token',token).send({token:token});
    }catch(err){
        return res.status(400).send({error:err,token:''});
    }
});
router.post('/login', async (req,res)=>{
    try{
        let user = await login(req.body);
        const token = jwt.sign({ _id:user._id },process.env.TOKEN_SECRET);
        return res.header('Auth-token',token).send({token:token});
    }catch(err){
        return res.status(400).send({error:err,token:''});
    }
});
module.exports = router;