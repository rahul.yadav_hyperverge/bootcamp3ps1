const router = require('express').Router();
const verify = require('../middleware/verifyToken');
const { User } = require('../models/user');
const { updateProfile } = require('../controllers/profile');

router.get('/getProfile', verify, async (req,res) =>{
    try{
        const user = await User.findById(req.user._id).select('-password');
        return res.send(user);
    }
    catch(err){
        return res.status(400).send('cant get the user');
    }
})
router.put('/updateProfile', verify , async (req,res) =>{
    try{
        const result = await updateProfile(req,res);
        return res.send('profile updated');
    }
    catch(err){
        return res.status(400).send('update on user are not done');
    }
});

module.exports = router;