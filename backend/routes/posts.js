const router = require('express').Router();
const verify = require('../middleware/verifyToken');
const { getPosts, addPost, addLike, getComments, addComment  } = require('../controllers/posts');

router.get('/', verify, async (req, res) => {
    try {
        const posts = await getPosts(req);
        return res.send(posts);
    }
    catch (err) {
        return res.status(400).send('cant get all post');
    }
})
router.post('/addPost', verify, async (req, res) => {
    try {
        const savedPost = await addPost(req,res);
        return res.send(savedPost);
    }
    catch (err) {
        return res.status(400).send(err);
    }
});
router.post('/post/addLike', verify, async (req, res) => {
    try {
        const postLiked = await addLike(req,res);
        return res.send('post Liked or disliked by the user');
    }
    catch (err) {
        return res.status(400).send('error occured during change the like status');
    }
})
router.post('/post/comments', verify, async (req, res) => {
    try {
        const comments = await getComments(req);
        return res.send(comments);
    }
    catch (err) {
        return res.status(400).send('cant get all post');
    }
})

router.post('/post/addComment', verify, async (req, res) => {
    try {
        const post = await addComment(req, res);
        return res.send('comment posted on the post');
    }
    catch (err) {
        return res.status(400).send('post not found');
    }
})
module.exports = router;
