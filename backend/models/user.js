const Joi = require('@hapi/joi');
const mongoose= require('mongoose');

const User = mongoose.model('User',
    new mongoose.Schema({
        name: {
            type: String,
            required: true,
            maxlength: 255
        },
        email: {
            type: String,
            required: true,
            minlength: 6,
            maxlength: 255,
            unique: true
        },
        phonenumber:{
            type: String,
            length: 10,
            default: ""
        },
        password: {
            type: String,
            required: true,
            minlength: 6,
            maxlength: 1024
        },
        photoUrl:{
            type: String,
            default:""
        },
        date: {
            type: Date,
            default: Date.now
        },
        friends:{
            type:Array,
        }
    })
);

function registrationValidation(user){
    const schema =  Joi.object({
        name: Joi.string().required().min(3).max(255).required(),
        email: Joi.string().email().required(),
        password: Joi.string().required().min(6).max(255).required()
    });
    return schema.validate(user);
};
function loginValidation(user){
    const schema = Joi.object({
        email: Joi.string().email().max(255).required(),
        password: Joi.string().min(6).max(255).required()
    })
    return schema.validate(user);
}
function profileValidation(user){
    const schema = Joi.object({
        email:Joi.string(),
        phonenumber:Joi.string(),
        photoUrl:Joi.string()
    });
    return schema.validate(user);
};
function friendValidation(friend){
    const schema = Joi.object({
        friendId: Joi.string().required()
    });
    return schema.validate(friend);
}

module.exports.User = User;
module.exports.loginValidation = loginValidation;
module.exports.registrationValidation = registrationValidation;
module.exports.profileValidation = profileValidation;
module.exports.friendValidation = friendValidation;
