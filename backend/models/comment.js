const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const Comment = mongoose.model('Comment',
    new mongoose.Schema({
        date: {
            type: Date,
            default: Date.now
        },
        createdBy: {
            type: String
        },
        comment: {
            type: String,
            required: true
        }
    })
);
function commentValidation(post) {
    const schema = Joi.object({
        postId: Joi.string().required(),
        comment: Joi.string().required()
    });
    return schema.validate(post);
}
module.exports.Comment = Comment;
module.exports.commentValidation = commentValidation;
