const Joi = require('@hapi/joi');
const mongoose = require('mongoose');
const Post = mongoose.model('Posts',
    new mongoose.Schema({
        date: {
            type: Date,
            default: Date.now
        },
        createdBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        postLink: {
            type: String,
            required: true
        },
        postDescription: {
            type: String,
            required: true
        },
        likes: {
            type: Number,
            default: 0,
            min: 0
        },
        likedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        comments: {
            type: Array,
            default: []
        }
    })
);

function postValidation(post) {
    const schema = Joi.object({
        postLink: Joi.string().required(),
        postDescription: Joi.string().required()
    });
    return schema.validate(post);
}
function postLikeValidation(post) {
    const schema = Joi.object({
        status: Joi.boolean().required(),
        postId: Joi.string().required()
    });
    return schema.validate(post);
}

module.exports.Post = Post;
module.exports.postValidation = postValidation;
module.exports.postLikeValidation = postLikeValidation;