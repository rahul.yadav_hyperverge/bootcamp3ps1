const express= require('express');
const app=express();
const mongoose=require('mongoose');
const dotenv= require('dotenv');
const cors = require('cors');

dotenv.config();

//Routes
const authRouter = require('./routes/auth');
const postRouter = require('./routes/posts');
const profileRouter = require('./routes/profile');
const friendRouter = require('./routes/friend');

//Connect to DB
mongoose.connect(process.env.DB_CONNECT,
    { useNewUrlParser: true ,useUnifiedTopology: true},
    ()=>console.log('Db is now connected...'));

//Middleware
app.use(express.json());
app.use(cors());

//Routes Middleware
app.use('/api/auth',authRouter);
app.use('/api/posts',postRouter);
app.use('/api/profile',profileRouter);
app.use('/api/friend',friendRouter);

const port=process.env.PORT || 5000;
app.listen(port,()=>console.log(`server listening to port ${port}...`));
