const { User, registrationValidation , loginValidation } = require('../models/user');
const bcrypt = require('bcryptjs');

async function register(body){
    const { error } = registrationValidation(body);
    if  (error)   return res.status(400).send({error:error.details[0].message,token:''});

    let user  = await User.findOne({ email:body.email });
    if  (user)  return res.status(400).send({error:"User Already registered",token:''});

    //hashing the password
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(body.password,salt);

    user= new User({
        name:body.name,
        email:body.email,
        password:hashPassword
    })
    return user;
}
async function login(body){
    const { error } = loginValidation(body);
    if (error)  return res.status(400).send({error:error.details[0].message,token:''});

    const user =await User.findOne({ email:body.email });
    if(!user) return res.status(400).send({error:'Invalid Email or Password',token:''});

    const validPassword =await bcrypt.compare(body.password,user.password);
    if(!validPassword)  return res.status(400).send({error:'Invalid Email or Password',token:''});
    
    return user;
}

module.exports.register = register;
module.exports.login = login;