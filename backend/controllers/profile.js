const { User, profileValidation, friendValidation } = require('../models/user');

async function updateProfile(req, res) {
    // const { error } = profileValidation(req.body);
    // if (error) return res.status(400).send('profile validation error');
    const user = await User.findById(req.user._id);
    console.log(user);
    if (req.body.email !== '')
        user.email = req.body.email;
    if (req.body.phonenumber !== '')
        user.phonenumber = req.body.phonenumber;
    if (req.body.photoUrl) {
        user.photoUrl = req.body.photoUrl;
    }
    await user.save();
}
async function friendSuggestion(req) {
    const users = await User.find().select(['-password', '-date', '-email', '-phonenumber']).limit(10);
    const currUser = await User.findById(req.user._id);
    let result = [];
    for(const user of users) {
        let userIsFriend = false;
        for(const friend of currUser.friends){
            if (friend === user._id.toString()) {
                userIsFriend = true;
                break;
            }
        }
        if (user._id.toString() !== req.user._id && !userIsFriend) {
            result.push(user);
        }
    }
    return result;
}

async function addFriend(req,res){
    const { error } = friendValidation(req.body);
    if(error) return res.status(400).send('friend validation is failed');

    let user = await User.findById(req.user._id);
    user.friends.push(req.body.friendId);
    await user.save();
}

module.exports.updateProfile = updateProfile;
module.exports.addFriend = addFriend;
module.exports.friendSuggestion = friendSuggestion;
