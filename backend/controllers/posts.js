const { Post, postValidation, postLikeValidation } = require('../models/post');
const { User } = require('../models/user');
const { Comment, commentValidation } = require('../models/comment');
async function getPosts(req) {
    const posts = await Post.find().populate('createdBy').exec();
    let result = [];
    posts.forEach(post => {
        let status = false;
        if (post.likedBy) {
            post.likedBy.forEach(likedby => {
                if (likedby === req.user._id) {
                    status = true;
                }
            });
        }
        result.push([post, status]);
    });
    return result;
}

async function addPost(req, res) {
    const { error } = postValidation(req.body);
    if (error) return res.status(400).send('post validation error');

    let user = await User.findById(req.user._id).select(['-password', '-phonenumber', '-email']);
    let post = new Post({
        createdBy: user,
        postLink: req.body.postLink,
        postDescription: req.body.postDescription
    });
    const savedPost = await post.save();
    return savedPost;
}

async function addLike(req, res) {
    const { error } = postLikeValidation(req.body);
    if (error) return res.status(400).send('post like validation errer');

    let post = await Post.findById(req.body.postId);
    if (req.body.status) {
        post.likes = post.likes + 1;
    } else {
        post.likes = post.likes - 1;
    }
    await post.save()
};

async function getComments(req) {
    const post = await Post.findById(req.body.postId);
    let comments = [];
    for (const comment of post.comments) {
        let user = await User.findById(comment.createdBy);
        comments.push([user.name, comment.comment,comment._id])
    }
    return comments;
}

async function addComment(req, res) {
    const { error } = commentValidation(req.body);
    if (error) return res.status(400).send('validation error in comment');

    const post = await Post.findById(req.body.postId);
    let comment = new Comment({
        createdBy:req.user._id,
        comment:req.body.comment
    })
    post.comments.push(comment);
    await post.save();
    return;
}

module.exports.addPost = addPost;
module.exports.addLike = addLike;
module.exports.getPosts = getPosts;
module.exports.addComment = addComment;
module.exports.getComments = getComments;